const std = @import("std");

fn lerp(a: f64, b: f64, amount: f64) f64 {
    return (b - a) * std.math.min(std.math.max(amount, 0.0), 1.0) + a;
}

fn dot(a: @Vector(2, f64), b: @Vector(2, f64)) f64 {
    const ab = a * b;
    return ab[0] + ab[1];
}

fn randomUnitVector(p: @Vector(2, f64)) @Vector(2, f64) {
    var seed = @bitCast(u64, p[0]) ^ @bitCast(u64, p[1]);
    const angle = std.rand.DefaultPrng.init(seed).random.float(f64) * 2.0 * std.math.pi;
    return .{std.math.cos(angle), std.math.sin(angle)};
}

fn dotUnitDelta(integral: @Vector(2, f64), p: @Vector(2, f64)) f64 {
    return dot(randomUnitVector(integral), p - integral);
}

fn perlin(p: @Vector(2, f64)) f64 {
    const p0 = @floor(p);
    const p1 = p0 + @splat(2, @floatCast(f64, 1));

    const delta = p - p0;

    var a = dotUnitDelta(p0, p);
    var b = dotUnitDelta(.{p1[0], p0[1]}, p);
    const lerped0 = lerp(a, b, delta[0]);

    a = dotUnitDelta(.{p0[0], p1[1]}, p);
    b = dotUnitDelta(p1, p);
    const lerped1 = lerp(a, b, delta[0]);

    return lerp(lerped0, lerped1, delta[1]);
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();

    const scale: f64 = 1.0 / 6.0;

    var random = std.rand.DefaultPrng.init(@intCast(u64, std.time.timestamp())).random;

    const offset: f64 = @intToFloat(f64, random.int(u32));

    var board: [35][35]u8 = undefined;

    for (board) |*row, y| {
        for (row) |*column, x| {
            const fertility = perlin(.{
                (@intToFloat(f64, x) + offset + 0.5) * scale,
                (@intToFloat(f64, y) + offset + 0.5) * scale,
            });
            if (fertility > 0) {
                column.* = 'O';
            } else {
                column.* = ' ';
            }
        }
    }

    while (true) {
        const pos = random.uintLessThan(usize, board.len * board[0].len);
        const at = &@ptrCast(*[board.len * board[0].len]u8, &board)[pos];
        if (at.* == 'O') {
            at.* = '#';
            break;
        }
    }

    for (board) |row| {
        try stdout.print("{}\n", .{row});
    }

    try stdout.print("\n", .{});

    while (true) {
        var fire_found = false;
        const board_back = board;
        for (board_back) |row, y| {
            for (row) |col, x| {
                if (col == '#') {
                    fire_found = true;

                    board[y][x] = '.';

                    if (y != 0 and board_back[y - 1][x] != '.' and board_back[y - 1][x] != ' ') {
                        board[y - 1][x] = '#';
                    }
                    if (x != board[0].len - 1 and board_back[y][x + 1] != '.' and board_back[y][x + 1] != ' ') {
                        board[y][x + 1] = '#';
                    }
                    if (y != board.len - 1 and board_back[y + 1][x] != '.' and board_back[y + 1][x] != ' ') {
                        board[y + 1][x] = '#';
                    }
                    if (x != 0 and board_back[y][x - 1] != '.' and board_back[y][x - 1] != ' ') {
                        board[y][x - 1] = '#';
                    }
                }
            }
        }
        if (fire_found) {
            for (board) |row| {
                try stdout.print("{}\n", .{row});
            }
            try stdout.print("\n", .{});
        } else {
            break;
        }
    }
}
