# Char Fire

Much like what the name implies, this is a California wildfire simulator using only ASCII characters.

For your viewing pleasure, please only use the latest and greatest version of the Zig standard, and run with a pager.

`zig run main.zig | less`
